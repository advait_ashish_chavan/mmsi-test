<?php

 $args = array(
 'orderby' => 'title',
 'post_type' => 'automobiles'
 );
 $the_query = new WP_Query( $args );
 
?>

 <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
 <div class="each-dr">
 <h1 class="the-title"><?php the_title() ;?></h1>
 </div>

 <?php endwhile; else: ?> <p>Sorry, there are no automobiles to display.</p> <?php endif; ?>
<?php wp_reset_query(); ?>
